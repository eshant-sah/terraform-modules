provider "aws" {
  region     = "us-east-1"
  access_key = ""
  secret_key = ""
}

module "my_vpc" {
  source = "../Module/vpc"
  vpc_cidr = "192.168.0.0/16"
  vpc_tenancy = "default"
  vpc_id = module.my_vpc.vpc_id
  subnet_cidr = "192.168.1.0/24"
}

module "my_ec2" {
    source = "../Module/ec2"
    aws_ami_id = "ami-013f17f36f8b1fefb"
    ec2_count = 1
    instance_type = "t2.micro"
    subnet_id  = module.my_vpc.subnet_id

}
